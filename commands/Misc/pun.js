import fetch from 'node-fetch';

import Command from '../../main/command.js';

export default class PunCommand extends Command
{
    constructor(main)
    {
        super(main, { name : 'pun', category : 'Misc', help : 'Random pun' });
    }
    run(message, _args, _prefix)
    {
        fetch('https://icanhazdadjoke.com/', {
            'headers' : {
                'User-Agent' :
                    'TrimmauDiscordBot (https://gitlab.com/Kibouo/TrimmauBot; HerokuHosted) node-fetch',
                'Accept' : 'text/plain'
            }
        })
            .then(res => res.text())
            .then(r => message.channel.send(r))
            .catch(console.log);
    }
}