import canvas_pkg from 'canvas';
const { createCanvas, Image } = canvas_pkg;

import fetch from 'node-fetch';
import discordjs_pkg from 'discord.js-light';
const { MessageAttachment } = discordjs_pkg;

import Command from '../../main/command.js';

export default class DangerousCommand extends Command
{
    constructor(main)
    {
        super(main, {
            name : 'dangerous',
            category : 'Image Generation',
            help : 'This is dangerous stuff!',
            args :
                [ { name : 'Text', desc : 'The text to put in the image.' } ],
            caseSensitive : true,
            cleanContent : true
        });
    }
    run(message, args, prefix)
    {
        if (!args)
            message.channel.send(`Error: No argument found. Please consult \`${
                prefix}help dangerous\` for more information.`);
        else
        {
            fetch('https://i.imgur.com/7uFWhYn.png')
                .then(res => res.buffer())
                .then(r => {
                    args          = args.join(' ');
                    const canvas  = createCanvas(483, 366);
                    const ctx     = canvas.getContext('2d');
                    const img_bg  = new Image();
                    img_bg.onload = function() {
                        ctx.drawImage(img_bg, 0, 0, 483, 366);
                        ctx.font      = 'bold 15px Arial';
                        ctx.fillStyle = 'white';

                        const x = 115;
                        let y   = 290;
                        ctx.rotate(-Math.PI / 36);
                        ctx.fillText(args.trim(), x, y);

                        message.channel.send('',
                                             new MessageAttachment(
                                                 canvas.toBuffer(),
                                                 ));
                    };
                    img_bg.src = r;
                });
        }
    }
}