import levenshtien from 'damerau-levenshtein';

import Command from '../../main/command.js';

// 5min
const COOLDOWN_MS = 300000;

export default class FGOQuizCommand extends Command
{
    constructor(main)
    {
        super(main, {
            name : 'quiz',
            category : 'Fate Grand Order',
            alias : [ 'fgo-quiz' ],
            help :
                'Get a quiz of a random servant. You have 5 minutes to answer!',
            args : [
                {
                    name : 'Skip',
                    desc :
                        'Optional. Skip the current question. Can be used once every 5 minutes.',
                    format : 'skip'
                },
            ]
        });
        this.quizStatus = {};
        this.prevSkip   = {};
    }
    run(message, args, _prefix)
    {
        if (args.length > 0 && args.includes('skip')
            && this.quizStatus[message.channel.id])
        {
            const now = new Date();
            if (now - (this.prevSkip[message.channel.id] || new Date(0))
                    > COOLDOWN_MS
                || message.author.id == this.main.config.ownerID)
            {
                message.channel.send('Okay, next question...');
                delete this.quizStatus[message.channel.id];
                this.prevSkip[message.channel.id] = now;
            }
            else
            {
                message.channel.send('You can only skip once every 5 minutes.');
                return;
            }
        }
        if (this.quizStatus[message.channel.id])
        {
            message.channel.send(
                'Another quiz is currently taking place. Please wait until it\'s done to start a new one.');
            return;
        }
        this.main.atlas_academy_api
            .fetch_item('na', 'servant', null, 'nice', true)
            .then(servant_data => {
                console.log(servant_data.name);

                let bio_text;
                do
                {
                    bio_text
                        = this.main.util.ARand(servant_data.profile.comments).comment;
                } while (
                    bio_text === 'It will be revealed when you complete a certain quest.'
                    || bio_text === '');

                // only replace parts of the name starting with a capital
                // letter. This prevents things such as 'of' and 'the' from
                // Hassan of the Cursed Arm's name from triggering the
                // replacement.
                // We match on non-ascii characters too. This is relevant for
                // example for "Scáthach-Skaði".

                /* eslint-disable-next-line no-control-regex */
                servant_data.name.match(/([A-Z])([^\u0000-\u007F]+|\w)+/g)
                    .forEach(name_part => {
                        bio_text = bio_text.replace(new RegExp(name_part, 'g'),
                                                    '[REDACTED]');
                    });
                const result = {
                    title : 'Which servant is this about?',
                    description : `\u200b\n${
                        bio_text}\n\nYou have 5 minutes to answer (case insensitive).`
                };

                this.quizStatus[message.channel.id] = true;
                message.channel.send('', { embed : result }).then(() => {
                    message.channel
                        .awaitMessages(
                            msg => levenshtien(servant_data.name.toLowerCase(),
                                               msg.content.toLowerCase())
                                       .similarity
                                   > 0.7,
                            {
                                max : 1,
                                time : COOLDOWN_MS,
                                errors : [ 'time' ]
                            })
                        .then(m => {
                            message.channel.send(`Congratulations! ${
                                m.first()
                                    .author} has got the correct answer! The answer is ${
                                servant_data.name} (ID: ${
                                servant_data.collectionNo}).`);
                            delete this.quizStatus[message.channel.id];
                        })
                        .catch(() => {
                            const time_since_last_skip
                                = new Date()
                                  - (this.prevSkip[message.channel.id]
                                     || new Date(0));
                            if (time_since_last_skip > COOLDOWN_MS
                                && this.quizStatus[message.channel.id])
                            {
                                message.channel.send(
                                    `5 minutes has passed, and no one has the answer. The correct answer is ${
                                        servant_data.name} (ID: ${
                                        servant_data.collectionNo}).`);
                                delete this.quizStatus[message.channel.id];
                            }
                        });
                });
            });
    }
}