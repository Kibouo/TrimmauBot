import timed_cache from 'memory-cache';
import Command from '../../main/command.js';

// higher rarities are pumped up a bit and lower rarities are balanced. This
// allows for less annoyance.
const RATES = {
    5 : 2,
    4 : 8,
    3 : 20,
    2 : 30,
    1 : 40
};
// 5min
const COOLDOWN_MS = 5 * 60 * 1000;

export default class FGOWaifuCommand extends Command
{
    constructor(main)
    {
        super(main, {
            name : 'servant',
            category : 'Fate Grand Order',
            help : 'Marry a random servant.',
            alias : [ 'fgo-waifu', 'waifu' ],
        });
        this.cooldown        = new timed_cache.Cache();
        this.weighted_rarity = main.util.weighted_table_generator(RATES);
    }

    fgoGacha()
    {
        const rarity = this.weighted_rarity();
        return this.main.atlas_academy_api
            .fetch_item('jp', 'servant', rarity, 'nice')
            .then(servant => {
                return {
                    name : servant.name,
                    rarity : rarity,
                    img_url : servant.extraAssets.charaGraph.ascension[4]
                };
            });
    }

    run(message, _args, _prefix)
    {
        let name = message.author.username;
        if (message.member) name = message.member.displayName;
        let time = this.cooldown.get(message.author.id)
                   - message.createdTimestamp + COOLDOWN_MS;
        if (time > 0 && message.author.id != this.main.config.ownerID)
        {
            message.channel.send(
                `You can only use this command once every 5 minutes. You can use it again in ${
                    Math.floor(time / 60000)} minutes ${
                    Math.ceil(time / 1000) % 60} seconds.`);
        }
        else
        {
            this.cooldown.put(
                message.author.id, message.createdTimestamp, COOLDOWN_MS);
            this.fgoGacha().then(servant => {
                message.channel.send('', {
                    embed : {
                        title : 'Congratulations!',
                        color : 0xff0000,
                        description :
                            `\u200b\nCongratulations! ${name} has married to ${
                                servant.name}! They have a rarity of ${
                                servant.rarity}, how lucky!`,
                        image : { url : servant.img_url }
                    }
                });
            });
        }
    }
}