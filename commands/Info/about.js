import Command from '../../main/command.js';
import {ATLAS_ACADEMY_URL, RAYSHIFT_URL} from '../../main/const.js';

export default class AboutCommand extends Command
{
    constructor(main)
    {
        super(main, {
            name : 'about',
            category : 'Bot Info',
            help : 'Get to know more about the bot.'
        });
    }
    run(message, _args, _prefix)
    {
        message.channel.send('', {
            embed : {
                title : 'TrimmauBot',
                description :
                    'TrimmauBot is an F/GO friend code bot. It is a fork of Aister\'s NobuBot.\n\u200b',
                fields : [
                    { name : 'Creator', value : 'Aister', inline : true },
                    { name : 'Customized by', value : 'Kibouo', inline : true },
                    {
                        name : 'Email',
                        value : 'csonka.mihaly@hotmail.com',
                        inline : true
                    },
                    ///
                    {
                        name : 'Engine',
                        value :
                            '[Discord.js light](https://github.com/timotejroiko/discord.js-light)',
                        inline : true
                    },
                    {
                        name : 'Support lookup',
                        value : `[Rayshift.io](${RAYSHIFT_URL})`,
                        inline : true
                    },
                    {
                        name : 'F/GO game data',
                        value : `[Atlas Academy](${ATLAS_ACADEMY_URL})`,
                        inline : true
                    },
                    ///
                    {
                        name : 'Source Code',
                        value : '[GitLab](https://gitlab.com/Kibouo/TrimmauBot)'
                    },
                ]
            }
        });
    }
}