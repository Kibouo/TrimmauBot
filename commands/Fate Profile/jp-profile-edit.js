import FGOProfileEdit from '../../main/cmds/profile-edit.js';

export default class FGOJPProfileEditCommand extends FGOProfileEdit
{
    constructor(main)
    {
        super(main, {
            name : 'jp-profile-edit',
            category : 'Fate Profile',
            help : 'Save or edit your F/GO profile (JP version).',
            args : [
                { name : 'Name', desc : 'Your IGN.', format : 'name:<IGN>' },
                {
                    name : 'ID',
                    desc : 'Your friend code.',
                    format : 'id:<123,456,789>'
                },
                {
                    name : 'Privacy',
                    desc :
                        'Optional. Defaults to `false`. If set to `false`, everyone can request to see your profile.',
                    format : 'privacy:[true|false]'
                },
                {
                    name : 'Support Image',
                    desc :
                        'The image showing your support list. Attach it to the message with your command.',
                    format : '<Image>'
                }
            ],
            caseSensitive : true
        });

        this.region      = 'jp';
        this.dbKeyPrefix = 'fgoProfile_';
    }
}