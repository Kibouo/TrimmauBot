import Command from '../../main/command.js';

export default class FGOUSProfileCommand extends Command
{
    constructor(main)
    {
        super(main, {
            name : 'us-profile',
            category : 'Fate Profile',
            alias : [ 'na-profile' ],
            help : 'Get your saved F/GO profile (NA version).',
            args : [ {
                name : 'Player',
                desc :
                    'Optional. Specify which user\'s profile to request. Their privacy setting has to be turned off.',
                format : '[@user|user_id]'
            } ],
        });
    }
    run(message, args, prefix)
    {
        let player = message.author.id;
        args       = args.join(' ');
        if (args)
        {
            let mentionID = args.match(/(?:<@!?)?(\d+)/);
            if (mentionID) player = mentionID[1];
        }
        Promise
            .all([
                this.main.db.get(`fgoUSProfile_${player}`),
                this.main.client.users.fetch(player)
            ])
            .then((profile) => {
                if (profile[0])
                {
                    profile[0] = JSON.parse(profile[0]);
                    if (!profile.privacy || !args)
                        message.channel.send('', {
                            embed : this.main.util.fgoProfile(profile[1],
                                                              profile[0])
                        });
                    else
                        message.channel.send(
                            'This player has set their privacy setting to true, thus the profile cannot be displayed.');
                }
                else if (args)
                    message.channel.send(
                        'Cannot find profile of requested player. Please recheck your arguments and try again.');
                else
                    message.channel.send(`Profile not found. Please use \`${
                        prefix}us-profile-edit\` to create one.`);
            });
    }
}