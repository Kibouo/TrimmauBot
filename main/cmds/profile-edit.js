import Command from '../../main/command.js';

export default class FGOProfileEdit extends Command
{
    parse_args(args)
    {
        // replace `|`. This was the old separator and people might still write
        // it out of habit.
        args
            = args.replace(/\|/g, ' ')
                  .split(
                      /((?:ign)|(?:name)|(?:id)|(?:support)|(?:privacy)) ?: ?/gi);
        args.splice(0, 1);
        args = this.main.util.chunk_array(args, 2);

        return args;
    }

    run(message, args, prefix)
    {
        args    = args.join(' ');
        let img = message.attachments.first();
        if (args || img)
        {
            this.main.db.get(`${this.dbKeyPrefix}${message.author.id}`)
                .then(profile => {
                    if (profile)
                        profile = JSON.parse(profile);
                    else
                        profile = {};

                    let modified = false;
                    args         = this.parse_args(args);

                    if (args)
                    {
                        args.forEach(([ k, v ]) => {
                            k = k.toLowerCase().trim();
                            // Saw some people invoking `ign:`.
                            // Don't they read help messages?
                            // Whatever, have it their way...
                            k = k === 'ign' ? 'name' : k;
                            v = v.trim();

                            modified = profile[k] !== v;
                            if (k === 'privacy')
                            {
                                profile[k] = v.toLowerCase() === 'true' ? true
                                                                        : false;
                            }
                            else
                            {
                                profile[k] = v;
                            }
                        });
                    }
                    if (img)
                    {
                        profile.support = img.url;
                        modified        = true;
                    }
                    if (modified)
                    {
                        console.log(profile);
                        this.main.db
                            .set(`${this.dbKeyPrefix}${message.author.id}`,
                                 JSON.stringify(profile))
                            .then(() => {
                                message.channel.send(
                                    'Profile saved successfully!', {
                                        embed : this.main.util.fgoProfile(
                                            message.author, profile)
                                    });
                            });
                    }
                    else
                        message.channel.send('No changes made.', {
                            embed : this.main.util.fgoProfile(message.author,
                                                              profile)
                        });
                });
        }
        else
        {
            message.channel.send(
                `Error: No argument provided. Please consult \`${prefix}help ${
                    this.region === 'na'
                        ? 'us-'
                        : 'jp-'}profile-edit\` for more information.`);
        }
    }
}