import fetch from 'node-fetch';
import {ATLAS_ACADEMY_API} from './const.js';

export default class AtlasAcademyHandler
{
    constructor(main)
    {
        this.main = main;

        this.jp_servants = [];
        this.na_servants = [];
        this.jp_ces      = [];
        this.na_ces      = [];
    }

    // I've tried doing this with Promise.all and process everything without
    // copy-pasting. Gave up after fighting JS for an hour.
    load_basic_data()
    {
        this.jp_servants
            = fetch(`${ATLAS_ACADEMY_API}export/JP/basic_servant.json`)
                  .then(res => res.json())
                  .then(r => {
                      return r.map(servant => {
                          return { id : servant.id, rarity : servant.rarity };
                      });
                  });
        this.na_servants
            = fetch(`${ATLAS_ACADEMY_API}export/NA/basic_servant.json`)
                  .then(res => res.json())
                  .then(r => {
                      return r.map(servant => {
                          return { id : servant.id, rarity : servant.rarity };
                      });
                  });
        this.jp_ces
            = fetch(`${ATLAS_ACADEMY_API}export/JP/basic_equip.json`)
                  .then(res => res.json())
                  .then(r => {
                      return r.map(servant => {
                          return { id : servant.id, rarity : servant.rarity };
                      });
                  });
        this.na_ces
            = fetch(`${ATLAS_ACADEMY_API}export/NA/basic_equip.json`)
                  .then(res => res.json())
                  .then(r => {
                      return r.map(servant => {
                          return { id : servant.id, rarity : servant.rarity };
                      });
                  });

        return this;
    }

    random_id(region, type, rarity)
    {
        region   = region == 'jp' ? region : 'na';
        type     = type == 'equip' ? type : 'servant';
        rarity   = rarity || this.main.util.rand(1, 5);
        let data = [];

        if (region === 'jp')
        {
            if (type === 'servant') { data = this.jp_servants; }
            else
            {
                data = this.jp_ces;
            }
        }
        else
        {
            if (type === 'servant') { data = this.na_servants; }
            else
            {
                data = this.na_ces;
            }
        }

        return data.then(data => {
            data = data.filter(item => item.rarity == rarity);
            return this.main.util.ARand(data).id;
        });
    }

    fetch_item(region, type, rarity, data_type, lore = false, id = null)
    {
        region           = region == 'jp' ? region : 'na';
        type             = type == 'equip' ? type : 'servant';
        rarity           = rarity || this.main.util.rand(1, 5);
        data_type        = data_type == 'nice' ? data_type : 'basic';
        lore             = lore ? 'true' : 'false';
        const id_promise = id ? Promise.resolve(id)
                              : this.random_id(region, type, rarity);

        return id_promise
            .then(id => {
                return fetch(`${ATLAS_ACADEMY_API}${data_type}/${
                    region.toUpperCase()}/${type}/${id}?lore=${lore}&lang=en`);
            })
            .then(r => r.json());
    }
}