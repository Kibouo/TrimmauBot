export const db    = 'https://raw.githubusercontent.com/aister/nobuDB/master/';
export const emoji = new Map([
    [ 'lol', 'http://i.imgur.com/NnuU2km.gif' ],
    [ 'huhu', 'http://i.imgur.com/Vpsng9m.png' ],
    [ 'rip', 'http://i.imgur.com/CXFDRZg.png' ],
    [ 'noob', 'http://i.imgur.com/v7r7fwG.gif' ],
    [ 'jam', 'http://i.imgur.com/ZXlW6fi.png' ],
    [ 'yorokobe', 'http://i.imgur.com/CGj0vTt.png' ],
    [ 'lewd', 'http://i.imgur.com/XH34as1.jpg' ],
    [ 'lenny', '( ͡° ͜ʖ ͡°)' ],
    [ 'tableflip', '(╯°□°）╯︵ ┻━┻' ],
    [ 'unflip', '┬─┬﻿ ノ( ゜-゜ノ)' ],
    [ 'shrug', '¯\\_(ツ)_/¯' ],
    [ 'police', 'http://i.imgur.com/YCxseg7.png' ]
]);
export const cirno = new Map([
    [ 'interested', 'http://i.imgur.com/mFGvw33.png' ],
    [ 'serious', 'http://i.imgur.com/FW9LtSk.png' ]
]);
export const RAYSHIFT_URL      = 'https://rayshift.io/';
export const RAYSHIFT_API      = `${RAYSHIFT_URL}api/v1/`;
export const ATLAS_ACADEMY_URL = 'https://atlasacademy.io/';
export const ATLAS_ACADEMY_API = 'https://api.atlasacademy.io/';