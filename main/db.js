import timed_cache from 'memory-cache';
import redis_pkg from 'redis';
const { createClient } = redis_pkg;

const CACHE_ITEM_LIFETIME_MS = 30 * 60 * 1000;

export default class Database
{
    constructor(dbURL)
    {
        this.cache  = new timed_cache.Cache();
        this.client = createClient(dbURL);
    }
    get(key)
    {
        return new Promise((resolve, reject) => {
            const cache_result = this.cache.get(key);
            if (cache_result) { resolve(cache_result); }
            else
            {
                this.client.get(key, (err, result) => {
                    if (err)
                    {
                        console.log(err);
                        reject(err);
                    }
                    this.cache.put(key, result, CACHE_ITEM_LIFETIME_MS);
                    resolve(result);
                });
            }
        });
    }
    set(key, value)
    {
        return new Promise((resolve, reject) => {
            this.client.set(key, value, (err) => {
                if (err)
                {
                    console.log(err);
                    reject(err);
                }
                this.cache.put(key, value, CACHE_ITEM_LIFETIME_MS);
                resolve(value);
            });
        });
    }
    del(key)
    {
        return new Promise((resolve, reject) => {
            this.client.del(key, (err) => {
                if (err)
                {
                    console.log(err);
                    reject(err);
                }
                this.cache.del(key);
                resolve();
            });
        });
    }
}
