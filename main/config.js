export default class Config
{
    constructor(option)
    {
        option = option || {};

        this.prefix        = option.prefix || process.env.PREFIX || '$';
        this.selfbot       = option.selfbot || process.env.SELFBOT || false;
        this.ownerID       = option.ownerID || process.env.OWNER_ID;
        this.token         = option.token || process.env.DISCORD_TOKEN;
        this.dbURL         = option.dbURL || process.env.DB_URL;
        this.rayshiftToken = option.rayshiftToken || process.env.RAYSHIFT_TOKEN
                             || '';
        this.herokuUrl = option.herokuUrl || process.env.HEROKU_URL || '';
    }
}